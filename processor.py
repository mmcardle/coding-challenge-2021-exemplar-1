"""
Code Word Processor
---------------------

    python3 processor.py out-example-2021-02-01-hansard-plenary.txt

or, if you would like a nicely-formatted HTML page to look at:

    ltldoorstep -o html --output-file output.html process sample_transcripts/out-example-2021-02-01-hansard-plenary.txt processor.py -e dask.threaded

This will create output.html in the current directory and, in a browser (tested with Chrome), should look like output.png.
"""

import sys
import logging

from dask.threaded import get

from spellchecker import SpellChecker

from ltldoorstep.processor import DoorstepProcessor
from ltldoorstep.reports.report import combine_reports
from ltldoorstep.document_utils import load_text, split_into_paragraphs

from spacy.lang.en import English

checker = SpellChecker()
nlp = English()
nlp.add_pipe('sentencizer')


def code_word_finder(paragraph, para_n, rprt):
    """
    Add report items to indicate where code words appear
    """

    paragraph_lower = paragraph.lower()
    
    doc = nlp(paragraph_lower)
    sentances = [sent.text.strip() for sent in doc.sents]

    code_words2 = []
    for sentance in sentances:
        words = sentance.split(" ")
        if words:
            candidate_code_word_letters = [word[0] for word in words if word]
            candidate_code_word = "".join(candidate_code_word_letters)
            code_words2.append((candidate_code_word, sentance))
    
    for code_word, sentance in code_words2:
        known_words = checker.known([code_word])
        for known_word in known_words:
            rprt.add_issue(
                logging.INFO,
                'code-word',
                f'Found the code word "{known_word}".',
                line_number=para_n,
                character_number=0,
                content=sentance
            )

    return rprt


class CodeWordFinderProcessor(DoorstepProcessor):
    preset = 'document'
    code = 'code-word-finder:1'
    description = "Code Word Finder for Lintol Coding Challenge"

    def get_workflow(self, filename, metadata={}):

        text = load_text(filename)

        paragraphs = split_into_paragraphs(text)

        workflow = {}
        
        output_task = [workflow_condense]
        for p_index, (paragraph, l) in enumerate(paragraphs):
            task_name = 'step-%s' % (p_index + 1)
            workflow[task_name] = (code_word_finder, paragraph, l, self.make_report(), )
            output_task.append(task_name)

        workflow['output'] = tuple(output_task)

        return workflow

# If there are several steps, this final function pulls them into one big report.
def workflow_condense(base, *args):
    return combine_reports(*args, base=base)


processor = CodeWordFinderProcessor.make


if __name__ == "__main__":
    argv = sys.argv
    processor = CodeWordFinderProcessor()
    processor.initialize()
    workflow = processor.build_workflow(argv[1])
    output = get(workflow, 'output')
    print(output)
